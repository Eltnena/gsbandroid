<?php

namespace androidBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class ConnexionController extends Controller
{
    public function connexionAction($log, $mdp)
    {
        $em = $this->getDoctrine()->getManager();
        $rp = $em->getRepository('androidBundle:Visiteur');
        
        $visi = $rp->findOneBy(array('visLogin' => $log , 'visMdp' => $mdp));
        
        
        return new JsonResponse($visi);
        
    }
}
