<?php

namespace androidBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;

class ConsulterController extends Controller{
    
    public function consulterCRAction(){
        return $this->render('@android/Default/consulterCR.html.twig');
    }
    
    public function lesCRAction($matricule){
        
        $em = $this->getDoctrine()->getManager();
        $rp = $em->getRepository('androidBundle:RapportVisite');
        // on veut tous les comptes rendus du visiteur
        $cr = $rp->findBy(array('visMatricule' => $matricule));
        
        return new JsonResponse($cr);
    }
    
    public function visiteurCRAction(){
        return $this->render('@android/Default/visiteurCR.html.twig');
    }
}
