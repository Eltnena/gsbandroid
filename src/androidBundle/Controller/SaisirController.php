<?php

namespace androidBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
/**
 * Description of SaisirController
 *
 * @author developpeur
 */
class SaisirController extends Controller {
       public function saisirAction() {
            return $this->render('@android/Default/saisir.html.twig');
       }
}
