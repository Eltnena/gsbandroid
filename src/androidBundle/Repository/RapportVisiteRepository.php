<?php

namespace androidBundle\Entity;

/**
 * Description of RapportVisiteRepository
 *
 * @author developpeur
 */
class RapportVisiteRepository extends \Doctrine\ORM\EntityRepository {
   
    public function findByRappVisiteur($matricule) {
        $queryBuilder = $this->createQueryBuilder('p');
        $queryBuilder->where ('p.visMatricule = :matricule')
                ->setParameter('matricule', $matricule);
        
        return $queryBuilder->getQuery()->getArrayResult();
    }
}
